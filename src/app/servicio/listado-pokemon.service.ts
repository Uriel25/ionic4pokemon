import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ListadoPokemonService {
  fav = [];
  constructor(private http: HttpClient) { }

  getdata(url){
    return this.http.get(`${url}`);
  }

  setfav(fav){
    this.fav.push(fav);
  }

  getfav(){
    return this.fav;
  }
}
