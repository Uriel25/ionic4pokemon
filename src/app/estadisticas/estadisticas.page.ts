import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListadoPokemonService } from '../servicio/listado-pokemon.service';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.page.html',
  styleUrls: ['./estadisticas.page.scss'],
})
export class EstadisticasPage implements OnInit {
  idPokemon: string;
  nombrePokemon: string;
  listado:any;
  constructor(private route: ActivatedRoute, private servicio: ListadoPokemonService) { }

  ionViewWillEnter(){
    this.idPokemon = this.route.snapshot.paramMap.get('id');
    this.nombrePokemon = this.route.snapshot.paramMap.get('nombre');

    this.servicio.getdata('https://pokeapi.co/api/v2/pokemon/' + this.idPokemon + '/').subscribe(data =>{
      console.log(data);
      this.listado = data;
    });
  }
  ngOnInit() {
  }

}
