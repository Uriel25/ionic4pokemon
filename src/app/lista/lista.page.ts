import { Component, OnInit } from '@angular/core';
import { ListadoPokemonService } from '../servicio/listado-pokemon.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  listado:any;
  constructor(private servicio: ListadoPokemonService) { }

  ngOnInit() {
    this.servicio.getdata('https://pokeapi.co/api/v2/pokemon/?offset=0&limit=964').subscribe(data =>{
      console.log(data);
      this.listado=data;
    });
  }

  favorite(id){
    this.servicio.setfav(id);
  }

}
